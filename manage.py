#!/usr/bin/python3
# -*- coding: Utf-8 -*
"""
co ownership management in blockchain mode
"""
# external module
import pygame as pg
from pygame.locals import *
# local module
from managePack import manage_constant as constancy

"""
home window  initialization
"""
pg.init()

# init home window
window = pg.display.set_mode((constancy.height_home, constancy.width_home))

# icon of the window
icon = pg.image.load(constancy.window_icon)
pg.display.set_icon (icon)

#title of the window
pg.display.set_caption(constancy.window_home_title)
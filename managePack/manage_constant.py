"""
constants of 'manage_co_ownership' 
"""
# home window parameter
width_home = 500
height_home = 320

# home window customization
window_home_title = "Gestion de copropriété" 
window_icon = "accounting/icon/blazon.png"